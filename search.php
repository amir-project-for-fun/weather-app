<!DOCTYPE html>
<html>

<head>
    <title>Weather App</title>
</head>

<?php  @include('link.php'); ?>

<body>

    <?php  @include('header.php'); ?>

    <div class="container">
        <div class="row m-5">
            <div class="col">
                <div class="d-flex">
                    <input class="form-control me-2" type="text" placeholder="Search" id="input-el">
                    <button class="btn btn-outline-primary" id="input-btn">Search</button>
                </div>
            </div>
        </div>
        <div class="row m-5 pt-5">
            <div class="col">
                <div class="fs-4 fw-bold" id="location">Bandar Baru Bangi <br> <b>Selangor</b></div>
                <img src="images/clear.png" style="height: 200px;width: 200px;" id="icon">
                <div class="display-2" id="temp">16C</div>
                <div class="fs-4" id="condition">Rainy</div>
            </div>
            <div class="col">
                <div class="row">
                    <div class="fs-4">Pressure</div>
                    <div class="display-2" id="pressure">16%</div>
                </div>
                <div class="row">
                    <div class="fs-4">Humidity</div>
                    <div class="display-2" id="humidity">16%</div>
                </div>
                <div class="row">
                    <div class="fs-4">Wind</div>
                    <div class="display-2" id="wind">16kmh</div>
                </div>
            </div>
        </div>
    </div>

<script src="index.js"></script>

</body>

</html>