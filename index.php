<!DOCTYPE html>
<html>
    <head>
        <title>Weather App</title>
    </head>

    <?php  @include('link.php'); ?>

    <body>

        <?php  @include('header.php'); ?>

        <div class="container-fluid text-center">
            
            <div class="row mt-5">
                <div class="col-4">
                    <div class="row">
                        <div class="col text-end"><div class="bi bi-alexa h1"></div></div>
                        <div class="col text-start" id="location">Bandar Baru Bangi <br> <b>Selangor</b></div>
                    </div>
                </div>
                <div class="col-4">
                    <img src="images/clear.png" style="height: 200px;width: 200px;" id="icon">
                    <div class="fs-4" id="condition">Rainy</div>
                    <div class="display-2" id="temp">16C</div>
                </div>
                <div class="col-4">
                    <div class="row">
                        <div class="col text-end" id="date">21 September 2023 <br> <b>Thursday</b></div>
                        <div class="col text-start"><div class="bi bi-calendar3 h1"></div></div>
                    </div>
                </div>
            </div>
            
            <div class="row justify-content-center align-items-center mt-5">
                <div class="col">
                    <img src="images/clear.png" style="height: 150px;width: 150px;" id="icon1">
                    <div class="fs-5" id="temp1">30C</div>
                    <div class="fs-5" id="time1">16:00</div>
                </div>
                <div class="col">
                    <img src="images/clear.png" style="height: 150px;width: 150px;" id="icon2">
                    <div class="fs-5" id="temp2">HELLO</div>
                    <div class="fs-5" id="time2">16:00</div>
                </div>
                <div class="col">
                    <img src="images/clear.png" style="height: 150px;width: 150px;" id="icon3">
                    <div class="fs-5" id="temp3">30C</div>
                    <div class="fs-5" id="time3">16:00</div>
                </div>
            </div>
        </div>
        
        <script src="index.js"></script>
    
    </body>
</html>