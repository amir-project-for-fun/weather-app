<header>
    <div class="collapse" id="navbarToggleExternalContent">
        <nav class="navbar navbar-expand-lg bg-body-tertiary">
            <div class="container-fluid">
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse justify-content-center" id="navbarNavAltMarkup">
                    <div class="navbar-nav">
                        <a class="nav-link active px-5" aria-current="page" href="index.php"><i class="bi bi-house h3"></i></a>
                        <a class="nav-link px-5" href="search.php"><i class="bi bi-search h3"></i></a>
                        <a class="nav-link px-5" href="dev.php"><i class="bi bi-person h3"></i></a>
                    </div>
                </div>
            </div>
        </nav>
    </div>

    <nav class="navbar" id="nav-el">
        <div class="container-fluid">
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarToggleExternalContent" aria-controls="navbarToggleExternalContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="display-6">Weather App</div>
            <div class="form-check form-switch">
                    <input class="form-check-input" type="checkbox" role="switch" id="changeEl" onclick="myFunction()">
                <label class="form-check-label" for="flexSwitchCheckDefault"></label>
            </div>
        </div>
    </nav>
</header>