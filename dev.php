<!DOCTYPE html>
<html>
<head>
    <title>Weather App</title>
</head>
<?php  @include('link.php'); ?>
<body>
<?php  @include('header.php'); ?>

    <div class="container">
        <div class="row m-5">
            <div class="col display-4 text-center">About Developers</div>
        </div>
        <div class="row mx-5 justify-content-center">
                <div class="card text-center" style="width: 18rem;">
                    <img src="images/profile.png" class="card-img-top">
                    <div class="card-body">
                        <h5 class="card-title">Amir Syafiq Ezrin</h5>
                        <p class="card-text">Software Developer</p>
                        <div class="row text-center">
                            <div class="col-4"><a href="" class="bi bi-instagram"></a></div>
                            <div class="col-4"><a href="" class="bi bi-linkedin"></a></div>
                            <div class="col-4"><a href="" class="bi bi-twitter-x"></a></div>
                        </div>
                    </div>
                </div>
        </div>
    </div>

<script src="index.js"></script>
</body>
</html>